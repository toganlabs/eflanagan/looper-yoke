import time
import adafruit_trellis_express
import board
import busio
import adafruit_adxl34x
import os

i2c = busio.I2C(board.ACCELEROMETER_SCL, board.ACCELEROMETER_SDA)
accelerometer = adafruit_adxl34x.ADXL345(i2c)

trellis = adafruit_trellis_express.TrellisM4Express(rotation=90)

# Change the overall brightness 0-1
trellis.pixels._neopixel.brightness = 1
bpm=128
trellis.pixels._neopixel.fill((0, 0, 0))
trellis.pixels._neopixel.show()

# step sequencer matrix
stepperrow=reversed(range(0,4))
steppercol=range(0,4)

# control matrix
ctrlrow=reversed(range(2,4))
ctrlcol=range(4,8)

# trackselect matrix
trackrow=reversed(range(0,2))
trackcol=range(4,8)


# synth matrix
synthrow=reversed(range(0,4))
synthcol=range(0,4)

# instrument matrix
instrrow=reversed(range(0,4))
instrcol=range(4,8)

# drum matrix
drumrow=reversed(range(0,4))
drumcol=range(4,8)


for row in ctrlrow:
    for col in ctrlcol:
        trellis.pixels[(row,col)]=(0,0,255)
        trellis.pixels._neopixel.show()

for row in trackrow:
    for col in trackcol:
        trellis.pixels[(row,col)]=(255,0,255)
        trellis.pixels._neopixel.show()


for row in stepperrow:
    for col in steppercol:
        trellis.pixels[(row,col)]=(255,255,255)
        trellis.pixels._neopixel.show()

initial = time.monotonic()
row=3
col=0
bpm=128


led_on = []

for x in range(0,4):
    led_on.append([])
    for y in range(0,8):
        led_on[x].append(False)

current_press = set()

# Master stepper track is 0. Instruments are 1-8
track = 0 

while True:
    pressed = set(trellis.pressed_keys)
    # Left (-10) & right (+10) like steering wheel
    bpm_add = accelerometer.acceleration[1]
    # Forward/nose dive (-10) & backward/pull up (+10)
    tilt_y = accelerometer.acceleration[0]
    # Like tilt_x but reversed when stable, otherwise detects up and down motion
    tilt_z = accelerometer.acceleration[2]
    
    pressed = set(trellis.pressed_keys)
 
    for press in pressed - current_press:
        x, y = press
        print(track)
        if not led_on[x][y]:
            if y<4:
                print("Turning on:", press)
                trellis.pixels[(x,y)]=(0,255,0)
                led_on[x][y] = True
            elif y>3 and x>1:
                print("Turning on:", press)
                trellis.pixels[(x,y)]=(0,255,255)
                led_on[x][y] = True
            elif y>3 and x<2:
                print("Turning on:", press)
                trellis.pixels[(x,y)]=(0,255,255)
                try:
                    oldx, oldy = track
                    trellis.pixels[(oldx,oldy)]=(255,0,255)
                except:
                    pass
                trellis.pixels[(x,y)]=(255,125,255)
                led_on[x][y] = True
                track=(x,y)
                
        else:
            if y<4:
                print("Turning off:", press)
                trellis.pixels[x, y] = (255,255,255)
                led_on[x][y] = False
            elif y>3 and x>1:
                print("Turning off:", press)
                trellis.pixels[x, y] = (0,0,255)
                led_on[x][y] = False
            elif y>3 and x<2:
                print("Turning off:", track)
                try:
                    oldx, oldy = track
                    trellis.pixels[(oldx,oldy)]=(255,0,255)
                except:
                    pass
                trellis.pixels[(x,y)]=(255,125,255)
                led_on[x][y] = True
                track=(x,y)

    current_press = pressed

    if (bpm_add > 1 or bpm_add < -1):
          bpm=bpm+((bpm_add*-1)/100)
    if bpm < 0:
       bpm = 1

    if time.monotonic()>initial+(60/bpm):
        trellis.pixels[(row,col)]=(255,0,0)
        trellis.pixels._neopixel.show()
        try:
            if (row,col) not in pressed and (row,col) not in current_press:
                if led_on[lastrow][lastcol]:
                    # play here
                    trellis.pixels[(lastrow,lastcol)]=(0,255,0)
                    trellis.pixels._neopixel.show()
                else:
                    # do not play here
                    trellis.pixels[(lastrow,lastcol)]=(255,255,255)
                    trellis.pixels._neopixel.show()

        except:
            pass
        lastcol=col
        lastrow=row
        col=col+1
        row=row
        if col>3:
            col=0
            row=row-1
        if row<0:
           row=3

        initial = time.monotonic()



